# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: tgros <tgros@student.42.fr>                +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/11/02 11:42:50 by tgros             #+#    #+#              #
#    Updated: 2018/11/15 16:56:24 by tgros            ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = ft_ssl
SRC_PATH = src/
INC_PATH = inc/
OBJ_PATH = obj/

DIGEST_PATH = digest/
MD5_PATH = md5/
SHA256_PATH = sha256/
SHA384_PATH = sha384/
SHA512_PATH = sha512/
WHIRLPOOL_PATH = whirlpool/
HELPER_PATH = helper/

VPATH = $(SRC_PATH) \
		$(SRC_PATH)$(DIGEST_PATH) \
		$(SRC_PATH)$(DIGEST_PATH)$(MD5_PATH) \
		$(SRC_PATH)$(DIGEST_PATH)$(SHA256_PATH) \
		$(SRC_PATH)$(DIGEST_PATH)$(SHA384_PATH) \
		$(SRC_PATH)$(DIGEST_PATH)$(SHA512_PATH) \
		$(SRC_PATH)$(DIGEST_PATH)$(WHIRLPOOL_PATH) \
		$(SRC_PATH)$(HELPER_PATH)

LIBFT_PATH = libft/
LIBFT_NAME = libft.a

HEADER_FILE = ft_ssl.h \
				digest.h \
				md5.h \
				sha256.h \
				sha384.h \
				sha512.h \
				whirlpool.h

SRC_NAME = 	main \
			md5 \
			md5_file_string \
			md5_stdin \
			sha256 \
			sha256_file_string \
			sha256_stdin \
			sha256_operations \
			sha384 \
			sha384_file_string \
			sha384_stdin \
			sha384_operations \
			sha512 \
			sha512_file_string \
			sha512_stdin \
			sha512_operations \
			whirlpool \
			whirlpool_file_string \
			whirlpool_stdin \
			whirlpool_operations \
			digest \
			parser \
			tasks \
			free \
			display \
			binary_operations \
			binary_rotations \
			file_operations

SRCNAME_PATH = $(addprefix $(OBJ_PATH), $(SRC_NAME))
HEADERNAME_PATH = $(addprefix $(INC_PATH), $(HEADER_FILE))
SRC = $(addsuffix $(EXT), $(SRC_NAME))
OBJ = $(addprefix $(OBJ_PATH), $(SRC:.c=.o))

EXT = .c
CC	= clang
ECHO = echo
FLG = -Werror -Wextra -Wall

MAKEFLAGS += --no-print-directory

all: $(NAME)

$(OBJ_PATH)%.o: %.c
	@/bin/mkdir -p $(OBJ_PATH)
	@$(CC) $(FLG) -I$(INC_PATH) -I$(LIBFT_PATH) -c -o $@ $^

$(NAME): $(OBJ) $(HEADERNAME_PATH)
	@make -C $(LIBFT_PATH)
	@$(CC) $(FLG) $(OBJ) $(LIBFT_PATH)$(LIBFT_NAME) -o $(NAME)
	@$(ECHO) $(NAME) compilation done

clean:
	@/bin/rm -rf $(OBJ_PATH)
	@make -C $(LIBFT_PATH) clean
	@$(ECHO) $(NAME) clean done

fclean: clean
	@/bin/rm -f $(NAME)
	@make -C $(LIBFT_PATH) fclean
	@$(ECHO) $(NAME) fclean done

re: fclean all

PHONY : re all clean fclean
