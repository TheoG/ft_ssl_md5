/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_error_exit.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/29 11:31:37 by tgros             #+#    #+#             */
/*   Updated: 2017/01/29 11:31:39 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** Puts the error message on the error output and exits the program.
*/

#include "libft.h"

void	ft_error_exit(char *error_msg)
{
	ft_putendl_fd(error_msg, 2);
	exit(-1);
}
