/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putstr_max.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/12 17:13:19 by tgros             #+#    #+#             */
/*   Updated: 2018/11/15 16:57:12 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_putstr_max(char const *s, size_t max)
{
	size_t	length;

	length = ft_strlen(s);
	if (!s)
		return ;
	write(1, s, length > max ? max : length);
}
