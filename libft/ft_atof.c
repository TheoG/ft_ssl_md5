/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atof.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <tgros@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/06 10:55:20 by tgros             #+#    #+#             */
/*   Updated: 2017/03/15 15:55:11 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

double	get_float(const char *str, long long *int_part, long long *float_part,
	int is_pos)
{
	long long	divisor;
	long long	is_after;

	is_after = 0;
	divisor = 1;
	while (*str)
	{
		if (ft_isdigit(*str))
			if (!is_after)
				*int_part = *int_part * 10 + *str - '0';
			else
			{
				*float_part = *float_part * 10 + *str - '0';
				divisor *= 10;
			}
		else if (*str == '.')
			if (is_after)
				return (is_pos * (*int_part + *float_part / (double)divisor));
			else
				is_after = 1;
		else
			return (is_pos * (*int_part + *float_part / (double)divisor));
		str++;
	}
	return (is_pos * (*int_part + *float_part / (double)divisor));
}

double	ft_atof(const char *str)
{
	int			is_pos;
	long long	int_part;
	long long	float_part;

	int_part = 0;
	float_part = 0;
	is_pos = 1;
	if (!str || !*str)
		return (0.0);
	while (*str && (*str == ' ' || *str == '\n' || *str == '\t' || *str == '\f'
	|| *str == '\r' || *str == '\v'))
		str++;
	if (*str == '-')
	{
		is_pos = -1;
		str++;
	}
	else if (*str == '+')
		str++;
	return (get_float(str, &int_part, &float_part, is_pos));
}
