/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstadd_end.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <tgros@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/29 17:56:25 by tgros             #+#    #+#             */
/*   Updated: 2018/11/12 12:42:27 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstadd_end(t_list **alst, t_list *new)
{
	t_list	*head;

	if (!new)
		return ;
	if (alst && !*alst)
	{
		*alst = new;
		new->next = NULL;
		return ;
	}
	head = *alst;
	while (head->next)
		head = head->next;
	head->next = new;
	new->next = NULL;
}

t_list	*ft_lstadd_end_fast(t_list *end, t_list *new)
{
	end->next = new;
	new->next = NULL;
	return (new);
}
