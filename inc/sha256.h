/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha256.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <tgros@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/10 13:21:40 by tgros             #+#    #+#             */
/*   Updated: 2018/11/16 00:31:51 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SHA256_H
# define SHA256_H

# define DEFAULT_H0 0x6a09e667
# define DEFAULT_H1 0xbb67ae85
# define DEFAULT_H2 0x3c6ef372
# define DEFAULT_H3 0xa54ff53a
# define DEFAULT_H4 0x510e527f
# define DEFAULT_H5 0x9b05688c
# define DEFAULT_H6 0x1f83d9ab
# define DEFAULT_H7 0x5be0cd19

# define SHA256_STATE_SIZE 8

static const	uint32_t	g_sha256_k[] = {
	0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5,
	0x3956c25b, 0x59f111f1, 0x923f82a4, 0xab1c5ed5,
	0xd807aa98, 0x12835b01, 0x243185be, 0x550c7dc3,
	0x72be5d74, 0x80deb1fe, 0x9bdc06a7, 0xc19bf174,
	0xe49b69c1, 0xefbe4786, 0x0fc19dc6, 0x240ca1cc,
	0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da,
	0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7,
	0xc6e00bf3, 0xd5a79147, 0x06ca6351, 0x14292967,
	0x27b70a85, 0x2e1b2138, 0x4d2c6dfc, 0x53380d13,
	0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85,
	0xa2bfe8a1, 0xa81a664b, 0xc24b8b70, 0xc76c51a3,
	0xd192e819, 0xd6990624, 0xf40e3585, 0x106aa070,
	0x19a4c116, 0x1e376c08, 0x2748774c, 0x34b0bcb5,
	0x391c0cb3, 0x4ed8aa4a, 0x5b9cca4f, 0x682e6ff3,
	0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208,
	0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2
};

typedef struct	s_sha256_state {
	uint32_t	buffer_size;
	uint32_t	hash_values[SHA256_STATE_SIZE];
	int32_t		bit_length;
	uint64_t	total_bit_length;
	bool		last_bit_placed;
}				t_sha256_state;

void			sha256_round(char *buffer, t_sha256_state *state, bool last);

void			sha256_transform(uint32_t *values, uint32_t *tmp_hash,
													uint32_t *words);
void			sha256_end_data(u_char *hash, t_sha256_state *state,
										bool last, uint32_t *tmp_hash);
void			sha256_setup_word(uint32_t *values, u_char *hash,
													uint32_t *words);

int32_t			ch(int32_t x, int32_t y, int32_t z);
int32_t			maj(int32_t x, int32_t y, int32_t z);
int32_t			sigma0(uint32_t x);
int32_t			sigma1(uint32_t x);

#endif
