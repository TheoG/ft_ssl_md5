/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ssl.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <tgros@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/02 13:13:33 by tgros             #+#    #+#             */
/*   Updated: 2018/11/16 12:57:31 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_SSL_H
# define FT_SSL_H

# include <sys/types.h>
# include <sys/stat.h>
# include <fcntl.h>
# include <stdint.h>
# include <stdbool.h>
# include <limits.h>
# include <errno.h>

# include "libft.h"
# include "digest.h"

static const char *g_digests[] = {
	"md5",
	"sha256",
	"sha384",
	"sha512",
	"whirlpool",
	0
};

void		*file_error_check(int *fd, t_task *task);

void		check_for_type(int argc, char *argv[]);
void		not_enough_args(void);
void		wrong_command(char *command);

void		binary_print(void *data, size_t n);
bool		check_endianness(void);
uint32_t	left_rotate(uint32_t f, uint32_t c);
uint32_t	right_rotate(uint32_t f, uint32_t c);

uint64_t	left_rotate_64(uint64_t f, uint64_t c);
uint64_t	right_rotate_64(uint64_t f, uint64_t c);

int32_t		int32_swap(int32_t x);
int64_t		int64_swap(int64_t x);
void		swap_bytes(void *pv, size_t n);

#endif
