/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   digest.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <tgros@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/03 19:16:43 by tgros             #+#    #+#             */
/*   Updated: 2018/11/16 13:46:13 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef DIGEST_H
# define DIGEST_H

# include "md5.h"
# include "sha256.h"
# include "sha384.h"
# include "sha512.h"
# include "whirlpool.h"

# define DIGEST_OPT_P 0b00000001
# define DIGEST_OPT_Q 0b00000010
# define DIGEST_OPT_R 0b00000100
# define DIGEST_OPT_S 0b00001000
# define DIGEST_OPT_STDIN 0b00010000
# define DIGEST_OPT_TEST 0b00100000

# define NUM_DIGEST_OPTS 2
# define NUM_FCT_PER_DIGEST 4

typedef	enum	e_digest {
	MD5,
	SHA256,
	SHA384,
	SHA512,
	WHIRLPOOL,
	DIGEST_E_END
}				t_digest;

typedef enum	e_digest_input_type {
	DIGEST_FILE,
	DIGEST_STDIN,
	DIGEST_STRING
}				t_digest_input_type;

typedef struct	s_task {
	char				*name;
	t_digest_input_type	input_type;
	t_digest			digest;
	int					options;
	char				error[64];
	void				*state;
	t_list				*stdin;
}				t_task;

typedef struct	s_digest_parsing {
	t_list		*tasks;
	int			options;
	t_digest	digest;
}				t_d_parsing;

static const char *g_digest_opts[] = {
	"-q",
	"-r",
	"-x",
	"-p",
	"-s"
};

static const int g_digest_opts_values[] = {
	DIGEST_OPT_Q,
	DIGEST_OPT_R,
	DIGEST_OPT_TEST
};
void			digest(t_digest digest, int argc, char *argv[]);

void			*md5_from_file(t_task *task, char *hash);
void			*md5_from_string(t_task *task, char *hash);
void			*md5_from_stdin(t_task *task, char *hash);
void			md5_stdin_main_loop(t_task *t, char *b, char *fb,
															uint32_t *s);
void			md5_stdin_finish(uint32_t size, char *fb, t_state *s,
															t_task *t);
void			md5_stdin_fb_round(t_task *task, char *buffer,
										char *fb, uint32_t *s);

void			*init_sha256_state(t_task *t, char *h);
void			*sha256_from_file(t_task *task, char *hash);
void			*sha256_from_string(t_task *task, char *hash);
void			*sha256_from_stdin(t_task *task, char *hash);
void			sha256_stdin_main_loop(t_task *t, char *buffer,
										char *fb, uint32_t *size);

void			*init_sha512_state(t_task *t, char *h);
void			*sha512_from_file(t_task *task, char *hash);
void			*sha512_from_string(t_task *task, char *hash);
void			*sha512_from_stdin(t_task *task, char *hash);
void			sha512_stdin_main_loop(t_task *t, char *buffer,
									char *fb, uint32_t *size);

void			*init_sha384_state(t_task *t, char *h);
void			*sha384_from_file(t_task *task, char *hash);
void			*sha384_from_string(t_task *task, char *hash);
void			*sha384_from_stdin(t_task *task, char *hash);
void			sha384_stdin_main_loop(t_task *t, char *buffer,
										char *fb, uint32_t *size);

void			*init_whirlpool_state(t_task *t, char *h);
void			*whirlpool_from_file(t_task *task, char *hash);
void			*whirlpool_from_string(t_task *task, char *hash);
void			*whirlpool_from_stdin(t_task *task, char *hash);
void			whirlpool_stdin_main_loop(t_task *t, char *buffer,
									char *fb, uint32_t *size);

t_d_parsing		parse(int argc, char *argv[], t_digest digest);
void			add_test_suite(t_d_parsing *parsing);
void			create_default_task(t_d_parsing *parsing);
void			create_stdin_file_tasks(char *argv, bool *first_file,
											t_d_parsing *parsing);
t_d_parsing		*create_string_tasks(char **argv, int argc,
									t_d_parsing *parsing, int *i);
t_task			new_task(char *name, t_digest_input_type type,
								t_d_parsing *parsing, char *error);
bool			search_options(t_d_parsing *parsing, bool first_file,
														char *argv);

void			display_digest(char *b, t_d_parsing parsing, size_t size,
														bool nl);
void			display_hexa_digest(char *buffer, size_t size,
					t_d_parsing *parsing, bool newline);
void			display_stdin(t_list *stdin_content);
void			display_name(t_task *task, t_d_parsing parsing, bool after);

void			execute(void *state, t_d_parsing parsing, int start_index);
void			execute_task(t_task *task, t_d_parsing parsing, int i,
														t_list *tmp);

void			free_list(t_list *list);

static void	*(*g_digest[])(t_task *task, char *hash) = {
	&init_md5_state,
	&md5_from_file,
	&md5_from_stdin,
	&md5_from_string,

	&init_sha256_state,
	&sha256_from_file,
	&sha256_from_stdin,
	&sha256_from_string,

	&init_sha384_state,
	&sha384_from_file,
	&sha384_from_stdin,
	&sha384_from_string,

	&init_sha512_state,
	&sha512_from_file,
	&sha512_from_stdin,
	&sha512_from_string,

	&init_whirlpool_state,
	&whirlpool_from_file,
	&whirlpool_from_stdin,
	&whirlpool_from_string
};

#endif
