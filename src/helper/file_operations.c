/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   file_operations.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <tgros@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/15 13:55:07 by tgros             #+#    #+#             */
/*   Updated: 2018/11/16 13:47:06 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"

void	*file_error_check(int *fd, t_task *task)
{
	if ((*fd = open(task->name, O_RDONLY)) == -1)
	{
		ft_putstr(g_digests[task->digest]);
		ft_putstr(": ");
		ft_putstr(task->name);
		ft_putstr(": ");
		ft_putstr(strerror(errno));
		ft_putchar('\n');
		return ((void*)1);
	}
	return (NULL);
}
