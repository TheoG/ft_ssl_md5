/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   binary_rotations.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <tgros@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/15 16:56:03 by tgros             #+#    #+#             */
/*   Updated: 2018/11/15 16:56:10 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"

uint32_t	left_rotate(uint32_t f, uint32_t c)
{
	return (((f) << (c)) | ((f) >> (32 - (c))));
}

uint32_t	right_rotate(uint32_t f, uint32_t c)
{
	return (((f) >> (c)) | ((f) << (32 - (c))));
}

uint64_t	left_rotate_64(uint64_t f, uint64_t c)
{
	return (((f) << (c)) | ((f) >> (64 - (c))));
}

uint64_t	right_rotate_64(uint64_t f, uint64_t c)
{
	return (((f) >> (c)) | ((f) << (64 - (c))));
}
