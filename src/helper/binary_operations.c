/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   binary_operations.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <tgros@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/10 13:34:12 by tgros             #+#    #+#             */
/*   Updated: 2018/11/15 16:56:35 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"
#include "libft.h"
#include <stdio.h>

bool		check_endianness(void)
{
	int		x;
	char	*y;

	x = 1;
	y = (char*)&x;
	return (*y + 48 == 1);
}

int32_t		int32_swap(int32_t x)
{
	return ((x >> 24) & 0xff) |
	((x << 8) & 0xff0000) |
	((x >> 8) & 0xff00) |
	((x << 24) & 0xff000000);
}

int64_t		int64_swap(int64_t x)
{
	return ((x & 0x00000000000000FFULL) << 56) |
	((x & 0x000000000000FF00ULL) << 40) |
	((x & 0x0000000000FF0000ULL) << 24) |
	((x & 0x00000000FF000000ULL) << 8) |
	((x & 0x000000FF00000000ULL) >> 8) |
	((x & 0x0000FF0000000000ULL) >> 24) |
	((x & 0x00FF000000000000ULL) >> 40) |
	((x & 0xFF00000000000000ULL) >> 56);
}

void		swap_bytes(void *pv, size_t n)
{
	char	*p;
	char	tmp;
	size_t	lo;
	size_t	hi;

	p = pv;
	lo = 0;
	hi = n - 1;
	while (hi > lo)
	{
		tmp = p[lo];
		p[lo] = p[hi];
		p[hi] = tmp;
		lo++;
		hi--;
	}
}
