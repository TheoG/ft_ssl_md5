/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tasks.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <tgros@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/14 16:17:37 by tgros             #+#    #+#             */
/*   Updated: 2018/11/16 15:27:44 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"

void		create_default_task(t_d_parsing *parsing)
{
	t_task	task;

	if (!parsing->tasks)
	{
		parsing->options |= DIGEST_OPT_STDIN;
		task = new_task(NULL, DIGEST_STDIN, parsing, NULL);
		ft_lstadd_end(&parsing->tasks, ft_lstnew(&task, sizeof(t_task)));
		parsing->options &= ~DIGEST_OPT_STDIN;
	}
}

t_d_parsing	*create_string_tasks(char **argv, int argc,
										t_d_parsing *parsing, int *i)
{
	t_task	task;

	if (argv[*i][2] == 0 && *i + 1 >= argc)
	{
		task = new_task(NULL, 0, parsing, "option requires an argument -- s");
		ft_lstadd_end(&parsing->tasks, ft_lstnew(&task, sizeof(t_task)));
		return (parsing);
	}
	else
	{
		if (argv[*i][2] == 0)
		{
			task = new_task(argv[*i + 1], DIGEST_STRING, parsing, NULL);
			ft_lstadd_end(&parsing->tasks, ft_lstnew(&task, sizeof(t_task)));
			(*i)++;
		}
		else
		{
			task = new_task(&argv[*i][2], DIGEST_STRING, parsing, NULL);
			ft_lstadd_end(&parsing->tasks, ft_lstnew(&task, sizeof(t_task)));
		}
	}
	return (NULL);
}

void		create_stdin_file_tasks(char *argv, bool *first_file,
												t_d_parsing *parsing)
{
	t_task	task;

	if (ft_strcmp(argv, g_digest_opts[3]) == 0 && !*first_file)
	{
		parsing->options |= DIGEST_OPT_P;
		task = new_task(argv, DIGEST_STDIN, parsing, NULL);
		ft_lstadd_end(&parsing->tasks, ft_lstnew(&task, sizeof(t_task)));
		parsing->options &= ~DIGEST_OPT_P;
	}
	else
	{
		task = new_task(argv, DIGEST_FILE, parsing, NULL);
		ft_lstadd_end(&parsing->tasks, ft_lstnew(&task, sizeof(t_task)));
		*first_file = true;
	}
}

void		add_test_suite(t_d_parsing *parsing)
{
	t_task	task;

	task = new_task("", DIGEST_STRING, parsing, NULL);
	ft_lstadd_end(&parsing->tasks, ft_lstnew(&task, sizeof(t_task)));
	task = new_task("a", DIGEST_STRING, parsing, NULL);
	ft_lstadd_end(&parsing->tasks, ft_lstnew(&task, sizeof(t_task)));
	task = new_task("abc", DIGEST_STRING, parsing, NULL);
	ft_lstadd_end(&parsing->tasks, ft_lstnew(&task, sizeof(t_task)));
	task = new_task("message digest", DIGEST_STRING, parsing, NULL);
	ft_lstadd_end(&parsing->tasks, ft_lstnew(&task, sizeof(t_task)));
	task = new_task("abcdefghijklmnopqrstuvwxyz",
								DIGEST_STRING, parsing, NULL);
	ft_lstadd_end(&parsing->tasks, ft_lstnew(&task, sizeof(t_task)));
	task = new_task("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxy\
z0123456789", DIGEST_STRING, parsing, NULL);
	ft_lstadd_end(&parsing->tasks, ft_lstnew(&task, sizeof(t_task)));
	task = new_task("1234567890123456789012345678901234567890123456789012345678\
9012345678901234567890", DIGEST_STRING, parsing, NULL);
	ft_lstadd_end(&parsing->tasks, ft_lstnew(&task, sizeof(t_task)));
}

t_task		new_task(char *name, t_digest_input_type type, t_d_parsing *parsing,
																	char *error)
{
	t_task	task;

	ft_bzero(task.error, 64);
	task.name = &name[0];
	task.input_type = type;
	task.options = parsing->options;
	task.digest = parsing->digest;
	task.stdin = NULL;
	ft_memcpy(task.error, error, ft_strlen(error));
	return (task);
}
