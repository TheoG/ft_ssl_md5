/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha512_operations.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <tgros@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/11 09:55:02 by tgros             #+#    #+#             */
/*   Updated: 2018/11/15 16:06:38 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"

int64_t	ch_sha512(int64_t x, int64_t y, int64_t z)
{
	return ((x & y) ^ (~x & z));
}

int64_t	maj_sha512(int64_t x, int64_t y, int64_t z)
{
	return (((x & y) ^ (x & z)) ^ (y & z));
}

int64_t	sigma0_sha512(uint64_t x)
{
	return (right_rotate_64(x, 28) ^
			right_rotate_64(x, 34) ^
			right_rotate_64(x, 39));
}

int64_t	sigma1_sha512(uint64_t x)
{
	return (right_rotate_64(x, 14) ^
			right_rotate_64(x, 18) ^
			right_rotate_64(x, 41));
}
