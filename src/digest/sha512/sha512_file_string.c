/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha512_file_string.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <tgros@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/15 16:08:43 by tgros             #+#    #+#             */
/*   Updated: 2018/11/16 12:57:17 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"

void	*sha512_from_file(t_task *task, char *hash)
{
	int				fd;
	char			buffer[128];
	t_sha512_state	state;

	if (file_error_check(&fd, task))
		return (false);
	state = *(t_sha512_state*)(task->state);
	ft_bzero(&buffer, 128);
	while ((state.bit_length = read(fd, &buffer, 128)) >= 0)
	{
		buffer[state.bit_length] = 0;
		state.bit_length *= 8;
		state.total_bit_length += state.bit_length;
		sha512_round(buffer, &state, state.bit_length == 0
								|| state.bit_length < 896);
		ft_bzero(&buffer, 128);
		if (state.bit_length == 0 || state.bit_length < 896)
		{
			ft_memcpy(hash, state.hash_values, 64);
			break ;
		}
	}
	close(fd);
	return ((void *)1);
}

void	*sha512_from_string(t_task *task, char *hash)
{
	t_sha512_state	state;
	char			buffer[128];
	int32_t			length;
	int32_t			cpy_len;

	state = *(t_sha512_state*)(task->state);
	ft_bzero(&buffer, 128);
	length = ft_strlen(task->name);
	cpy_len = 0;
	while (length >= 0)
	{
		ft_bzero(buffer, 128);
		ft_memcpy(buffer, task->name + cpy_len, (length > 128 ? 128 : length));
		cpy_len += (length > 128 ? 128 : length);
		state.bit_length = (length >= 128 ? 128 : length) * 8;
		if (state.bit_length < 0)
			state.bit_length = 0;
		state.total_bit_length += state.bit_length;
		sha512_round(buffer, &state, length < 112);
		if (state.last_bit_placed && length < 112)
			break ;
		length = length - 128 < 0 ? 0 : length - 128;
	}
	ft_memcpy(hash, state.hash_values, 128);
	return ((void *)1);
}
