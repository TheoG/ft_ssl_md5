/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha512_stdin.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <tgros@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/10 16:00:12 by tgros             #+#    #+#             */
/*   Updated: 2018/11/16 00:14:09 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"

void	*sha512_from_stdin(t_task *task, char *hash)
{
	char				buffer[128];
	char				full_buffer[128];
	uint32_t			remaining_size;
	t_sha512_state		*state;

	state = (t_sha512_state*)(task->state);
	remaining_size = sizeof(full_buffer);
	ft_bzero(&buffer, 128);
	ft_bzero(&full_buffer, 128);
	sha512_stdin_main_loop(task, buffer, full_buffer, &remaining_size);
	state->bit_length = (128 - remaining_size) * 8;
	state->total_bit_length += state->bit_length;
	sha512_round(full_buffer, state, remaining_size > 16);
	ft_lstadd_end(&task->stdin, ft_lstnew(full_buffer, state->bit_length / 8));
	if (remaining_size <= 16)
	{
		ft_bzero(full_buffer, 128);
		state->bit_length = 0;
		sha512_round(full_buffer, state, true);
	}
	ft_memcpy(hash, state->hash_values, 64);
	return ((void *)1);
}

void	sha512_stdin_full_buf(t_task *t, uint32_t *size, char *buffer, char *fb)
{
	int				second_copy_part;
	t_sha512_state	*s;

	s = (t_sha512_state*)t->state;
	second_copy_part = s->bit_length - *size;
	ft_memcpy(fb + 128 - *size, buffer, *size);
	s->bit_length = 1024;
	s->total_bit_length += 1024;
	sha512_round(buffer, s, false);
	s->bit_length = second_copy_part;
	ft_lstadd_end(&t->stdin, ft_lstnew(fb, 128));
	ft_memcpy(fb, buffer, second_copy_part);
	*size = 128 - second_copy_part;
}

void	sha512_stdin_main_loop(t_task *t, char *buffer,
											char *fb, uint32_t *size)
{
	t_sha512_state	*s;

	s = (t_sha512_state*)t->state;
	while ((s->bit_length = read(0, buffer, 128)))
	{
		while (s->bit_length > 0)
		{
			if ((int)(*size - s->bit_length) > 0)
			{
				ft_memcpy(fb + 128 - *size, buffer, s->bit_length);
				*size -= s->bit_length;
				s->bit_length = 0;
			}
			else
			{
				sha512_stdin_full_buf(t, size, buffer, fb);
			}
		}
	}
}
