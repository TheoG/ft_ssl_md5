/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha256_operations.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <tgros@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/11 09:55:02 by tgros             #+#    #+#             */
/*   Updated: 2018/11/14 22:27:05 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"

int32_t	ch(int32_t x, int32_t y, int32_t z)
{
	return ((x & y) ^ (~x & z));
}

int32_t	maj(int32_t x, int32_t y, int32_t z)
{
	return (((x & y) ^ (x & z)) ^ (y & z));
}

int32_t	sigma0(uint32_t x)
{
	return (right_rotate(x, 2) ^ right_rotate(x, 13) ^ right_rotate(x, 22));
}

int32_t	sigma1(uint32_t x)
{
	return (right_rotate(x, 6) ^ right_rotate(x, 11) ^ right_rotate(x, 25));
}
