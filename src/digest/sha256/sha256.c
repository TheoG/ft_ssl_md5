/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha256.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <tgros@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/02 13:11:49 by tgros             #+#    #+#             */
/*   Updated: 2018/11/16 15:35:14 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"

void	*init_sha256_state(t_task *task, char *hash)
{
	t_sha256_state	*state;

	(void)task;
	(void)hash;
	if (!(state = (t_sha256_state*)malloc(sizeof(t_sha256_state))))
	{
		ft_error_exit("Can't allocate the hash state");
	}
	state->hash_values[0] = DEFAULT_H0;
	state->hash_values[1] = DEFAULT_H1;
	state->hash_values[2] = DEFAULT_H2;
	state->hash_values[3] = DEFAULT_H3;
	state->hash_values[4] = DEFAULT_H4;
	state->hash_values[5] = DEFAULT_H5;
	state->hash_values[6] = DEFAULT_H6;
	state->hash_values[7] = DEFAULT_H7;
	state->bit_length = 0;
	state->total_bit_length = 0;
	state->last_bit_placed = false;
	state->buffer_size = SHA256_STATE_SIZE;
	return (state);
}

void	sha256_round(char *buffer, t_sha256_state *state, bool last)
{
	u_char		hash[64];
	uint32_t	tmp_hash[SHA256_STATE_SIZE];
	uint32_t	words[64];
	uint32_t	values[8];

	ft_bzero(hash, 64);
	ft_memcpy(hash, buffer, state->bit_length / 8);
	sha256_end_data(hash, state, last, tmp_hash);
	ft_bzero(values, sizeof(values));
	sha256_setup_word(values, hash, words);
	sha256_transform(values, tmp_hash, words);
	state->hash_values[0] += tmp_hash[0];
	state->hash_values[1] += tmp_hash[1];
	state->hash_values[2] += tmp_hash[2];
	state->hash_values[3] += tmp_hash[3];
	state->hash_values[4] += tmp_hash[4];
	state->hash_values[5] += tmp_hash[5];
	state->hash_values[6] += tmp_hash[6];
	state->hash_values[7] += tmp_hash[7];
}

void	sha256_setup_word(uint32_t *values, u_char *hash, uint32_t *words)
{
	while (values[6] < 16)
	{
		words[values[6]] = ((uint32_t)hash[values[7]] << 24) |
							(((uint32_t)hash[values[7] + 1]) << 16) |
							(((uint32_t)hash[values[7] + 2]) << 8) |
							(((uint32_t)hash[values[7] + 3]));
		values[7] += 4;
		values[6]++;
	}
	while (values[6] < 64)
	{
		values[0] = (right_rotate(words[values[6] - 15], 7)) ^
					(right_rotate(words[values[6] - 15], 18)) ^
					(words[values[6] - 15] >> 3);
		values[1] = (right_rotate(words[values[6] - 2], 17)) ^
					(right_rotate(words[values[6] - 2], 19)) ^
					(words[values[6] - 2] >> 10);
		words[values[6]] = words[values[6] - 16] + values[0] +
							words[values[6] - 7] + values[1];
		values[6]++;
	}
}

void	sha256_end_data(u_char *hash, t_sha256_state *state, bool last,
													uint32_t *tmp_hash)
{
	int64_t	swapped;

	if (state->bit_length / 8 < 64 && !state->last_bit_placed)
	{
		hash[state->bit_length / 8] = (char)(1 << 7);
		state->last_bit_placed = true;
	}
	if (last)
	{
		ft_memcpy(hash + 56, &state->total_bit_length, 8);
		swapped = int64_swap(state->total_bit_length);
		ft_memcpy(hash + 56, &swapped, 8);
	}
	ft_memcpy(tmp_hash, state->hash_values, 4 * SHA256_STATE_SIZE);
}

void	sha256_transform(uint32_t *values, uint32_t *tmp_hash, uint32_t *words)
{
	values[6] = 0;
	while (values[6] < 64)
	{
		values[1] = sigma1(tmp_hash[4]);
		values[2] = ch(tmp_hash[4], tmp_hash[5], tmp_hash[6]);
		values[3] = tmp_hash[7] + values[1] + values[2] +
					g_sha256_k[values[6]] + words[values[6]];
		values[0] = sigma0(tmp_hash[0]);
		values[4] = maj(tmp_hash[0], tmp_hash[1], tmp_hash[2]);
		values[5] = values[0] + values[4];
		tmp_hash[7] = tmp_hash[6];
		tmp_hash[6] = tmp_hash[5];
		tmp_hash[5] = tmp_hash[4];
		tmp_hash[4] = tmp_hash[3] + values[3];
		tmp_hash[3] = tmp_hash[2];
		tmp_hash[2] = tmp_hash[1];
		tmp_hash[1] = tmp_hash[0];
		tmp_hash[0] = values[3] + values[5];
		values[6]++;
	}
}
