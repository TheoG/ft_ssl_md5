/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha256_file_string.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <tgros@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/15 13:51:42 by tgros             #+#    #+#             */
/*   Updated: 2018/11/16 12:57:23 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"

void	*sha256_from_file(t_task *task, char *hash)
{
	int				fd;
	char			buffer[64];
	t_sha256_state	state;

	if (file_error_check(&fd, task))
		return (false);
	state = *(t_sha256_state*)(task->state);
	ft_bzero(&buffer, 64);
	while ((state.bit_length = read(fd, &buffer, 64)) >= 0)
	{
		buffer[state.bit_length] = 0;
		state.bit_length *= 8;
		state.total_bit_length += state.bit_length;
		sha256_round(buffer, &state, state.bit_length == 0
								|| state.bit_length < 448);
		ft_bzero(&buffer, 64);
		if (state.bit_length == 0 || state.bit_length < 448)
		{
			ft_memcpy(hash, state.hash_values, 32);
			break ;
		}
	}
	close(fd);
	return ((void *)1);
}

void	*sha256_from_string(t_task *task, char *hash)
{
	t_sha256_state	state;
	char			buffer[64];
	int32_t			len;
	int32_t			cpy_length;

	state = *(t_sha256_state*)(task->state);
	ft_bzero(&buffer, 64);
	len = ft_strlen(task->name);
	cpy_length = 0;
	while (len >= 0)
	{
		ft_bzero(buffer, 64);
		ft_memcpy(buffer, task->name + cpy_length, (len > 64 ? 64 : len));
		cpy_length += (len > 64 ? 64 : len);
		state.bit_length = (len >= 64 ? 64 : len) * 8;
		if (state.bit_length < 0)
			state.bit_length = 0;
		state.total_bit_length += state.bit_length;
		sha256_round(buffer, &state, len < 56);
		if (state.last_bit_placed && len < 56)
			break ;
		len = len - 64 < 0 ? 0 : len - 64;
	}
	ft_memcpy(hash, state.hash_values, 32);
	return ((void *)1);
}
