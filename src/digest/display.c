/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   display.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <tgros@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/14 16:53:37 by tgros             #+#    #+#             */
/*   Updated: 2018/11/15 17:36:10 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"

void	display_digest(char *buffer, t_d_parsing parsing, size_t size,
															bool newline)
{
	uint8_t		*p;
	uint32_t	i;
	int32_t		swapped32;
	int64_t		swapped64;

	i = 0;
	p = (uint8_t *)&(buffer[0]);
	while (i < size && parsing.digest != MD5)
	{
		if (parsing.digest == SHA256)
			swapped32 = int32_swap(*((int32_t*)p));
		else
			swapped64 = int64_swap(*((int64_t*)p));
		ft_memcpy(p, parsing.digest == SHA256 ? (void *)&swapped32 :
			(void *)&swapped64, parsing.digest == SHA256 ? 4 : 8);
		i += parsing.digest == SHA256 ? 4 : 8;
		p += parsing.digest == SHA256 ? 4 : 8;
	}
	display_hexa_digest(buffer, size, &parsing, newline);
}

void	display_hexa_digest(char *buffer, size_t size, t_d_parsing *parsing,
															bool newline)
{
	uint32_t	i;
	uint8_t		*p;
	char		*hex_str;

	i = 0;
	p = (uint8_t *)&(buffer[0]);
	while (i < size)
	{
		if (*p < 16)
			ft_putchar('0');
		hex_str = ft_itoa_base(*p, 16);
		ft_putstr(hex_str);
		free(hex_str);
		p += sizeof(uint8_t);
		i++;
	}
	if (newline || (parsing->options & DIGEST_OPT_Q))
		ft_putchar('\n');
	else
		ft_putchar(' ');
}

void	display_stdin(t_list *stdin_content)
{
	t_list	*tmp;

	tmp = stdin_content;
	while (tmp)
	{
		ft_putstr_max(tmp->content, tmp->content_size);
		tmp = tmp->next;
	}
}

void	display_name(t_task *task, t_d_parsing parsing, bool after)
{
	if (!after)
	{
		ft_putstr_upper(g_digests[parsing.digest]);
		ft_putstr(" (");
	}
	if (task->input_type == DIGEST_STRING)
		ft_putchar('"');
	ft_putstr(task->name);
	if (task->input_type == DIGEST_STRING)
		ft_putchar('"');
	if (!after)
	{
		ft_putstr(") = ");
	}
	else
	{
		ft_putchar('\n');
	}
}
