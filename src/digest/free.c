/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <tgros@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/14 16:49:00 by tgros             #+#    #+#             */
/*   Updated: 2018/11/14 17:19:49 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"

void	free_list(t_list *list)
{
	t_list	*tmp;
	t_list	*to_delete;

	tmp = list;
	while (tmp)
	{
		free(tmp->content);
		to_delete = tmp;
		tmp = tmp->next;
		free(to_delete);
	}
}
