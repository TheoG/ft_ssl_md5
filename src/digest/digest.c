/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   digest.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <tgros@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/11 14:24:35 by tgros             #+#    #+#             */
/*   Updated: 2018/11/16 15:26:57 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"

void	digest(t_digest digest, int argc, char *argv[])
{
	void		*state;
	int			start_index;
	t_d_parsing	parsing;

	start_index = (int)digest * NUM_FCT_PER_DIGEST;
	state = g_digest[(int)start_index](NULL, NULL);
	parsing = parse(argc, argv, digest);
	parsing.digest = digest;
	execute(state, parsing, start_index);
	free(state);
}

void	execute(void *state, t_d_parsing parsing, int start_index)
{
	t_list	*tmp;
	t_list	*to_delete;
	t_task	*task;

	tmp = parsing.tasks;
	while (tmp)
	{
		task = (t_task*)tmp->content;
		task->state = state;
		if (ft_strlen(task->error) > 0)
		{
			ft_putstr(g_digests[parsing.digest]);
			ft_putstr(": ");
			ft_putendl(task->error);
		}
		else
			execute_task(task, parsing, start_index, tmp);
		to_delete = tmp;
		tmp = tmp->next;
	}
	free_list(parsing.tasks);
}

void	execute_task(t_task *task, t_d_parsing parsing, int start_index,
															t_list *tmp)
{
	void	*success;
	char	final_hash[512];

	success = NULL;
	ft_bzero(final_hash, 512);
	if ((success = g_digest[start_index + (int)task->input_type + 1](task,
													final_hash)) != NULL)
	{
		if (!(task->options & DIGEST_OPT_Q) && !(task->options & DIGEST_OPT_R)
	&& !(task->options & DIGEST_OPT_P) && !(task->options & DIGEST_OPT_STDIN))
		{
			display_name(tmp->content, parsing, false);
		}
		if (task->options & DIGEST_OPT_P)
		{
			display_stdin(task->stdin);
			free_list(task->stdin);
		}
		display_digest(final_hash, parsing, *((uint32_t*)&task->state[0]) * 4,
											!(task->options & DIGEST_OPT_R));
	}
	if (success && !(task->options & DIGEST_OPT_Q) &&
	task->options & DIGEST_OPT_R && !(task->options & DIGEST_OPT_STDIN))
		display_name(tmp->content, parsing, true);
}
