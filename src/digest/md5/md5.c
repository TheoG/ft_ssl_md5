/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   md5.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <tgros@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/02 13:11:49 by tgros             #+#    #+#             */
/*   Updated: 2018/11/16 15:35:01 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"

void	*init_md5_state(t_task *task, char *hash)
{
	t_state	*state;

	(void)task;
	(void)hash;
	if (!(state = (t_state*)malloc(sizeof(t_state))))
		ft_error_exit("Can't allocate the hash state");
	state->hash_values[0] = DEFAULT_A0;
	state->hash_values[1] = DEFAULT_B0;
	state->hash_values[2] = DEFAULT_C0;
	state->hash_values[3] = DEFAULT_D0;
	state->bit_length = 0;
	state->total_bit_length = 0;
	state->last_bit_placed = false;
	state->buffer_size = MD5_STATE_SIZE;
	return (state);
}

void	md5_round(char *buffer, t_state *state, bool last)
{
	u_char		hash[64];
	uint32_t	tmp_hash[MD5_STATE_SIZE];
	uint32_t	words[16];
	uint32_t	values[4];

	ft_bzero(hash, 64);
	ft_bzero(values, sizeof(values));
	ft_memcpy(hash, buffer, state->bit_length / 8);
	if (state->bit_length / 8 < 64 && !state->last_bit_placed)
	{
		hash[state->bit_length / 8] = (char)(1 << 7);
		state->last_bit_placed = true;
	}
	if (last)
		ft_memcpy(hash + 56, &state->total_bit_length, 8);
	ft_memcpy(tmp_hash, state->hash_values, sizeof(tmp_hash));
	while (values[2] < 16)
	{
		words[values[2]] = ((uint32_t)hash[values[3]]) | (((uint32_t)
		hash[values[3] + 1]) << 8) | (((uint32_t)hash[values[3] + 2]) << 16) |
		(((uint32_t)hash[values[3] + 3]) << 24);
		values[3] += 4;
		values[2]++;
	}
	update_hash_round(values, tmp_hash, state, words);
}

void	update_hash_round(uint32_t *values, uint32_t *hash,
							t_state *state, uint32_t *words)
{
	values[2] = 0;
	while (values[2] < 64)
	{
		get_f_and_g_values(hash, values);
		values[0] = values[0] + hash[0] + g_md5_k[values[2]] + words[values[1]];
		hash[0] = hash[3];
		hash[3] = hash[2];
		hash[2] = hash[1];
		hash[1] = hash[1] + left_rotate(values[0], g_md5_s[values[2]]);
		values[2]++;
	}
	state->hash_values[0] += hash[0];
	state->hash_values[1] += hash[1];
	state->hash_values[2] += hash[2];
	state->hash_values[3] += hash[3];
}

void	get_f_and_g_values(uint32_t *hash, uint32_t *values)
{
	if (values[2] < 16)
	{
		values[0] = (hash[1] & hash[2]) | ((~hash[1]) & hash[3]);
		values[1] = values[2];
	}
	else if (values[2] < 32)
	{
		values[0] = (hash[3] & hash[1]) | ((~hash[3]) & hash[2]);
		values[1] = (5 * values[2] + 1) % 16;
	}
	else if (values[2] < 48)
	{
		values[0] = hash[1] ^ hash[2] ^ hash[3];
		values[1] = (3 * values[2] + 5) % 16;
	}
	else
	{
		values[0] = hash[2] ^ (hash[1] | (~hash[3]));
		values[1] = (7 * values[2]) % 16;
	}
}
