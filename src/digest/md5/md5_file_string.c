/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   md5_file_string.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <tgros@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/14 17:22:30 by tgros             #+#    #+#             */
/*   Updated: 2018/11/16 12:57:26 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"

void	*md5_from_file(t_task *task, char *hash)
{
	int		fd;
	char	buffer[64];
	t_state	state;

	if (file_error_check(&fd, task))
		return (false);
	state = *(t_state*)(task->state);
	ft_bzero(&buffer, 64);
	while ((state.bit_length = read(fd, &buffer, 64)) >= 0)
	{
		buffer[state.bit_length] = 0;
		state.bit_length *= 8;
		state.total_bit_length += state.bit_length;
		md5_round(buffer, &state, state.bit_length == 0
								|| state.bit_length < 448);
		ft_bzero(&buffer, 64);
		if (state.bit_length == 0 || state.bit_length < 448)
		{
			ft_memcpy(hash, state.hash_values, 16);
			break ;
		}
	}
	close(fd);
	return ((void *)1);
}

void	*md5_from_string(t_task *task, char *hash)
{
	t_state	state;
	char	buffer[64];
	int32_t length;
	int32_t	copied_l;

	state = *(t_state*)(task->state);
	ft_bzero(&buffer, 64);
	length = ft_strlen(task->name);
	copied_l = 0;
	while (length >= 0)
	{
		ft_bzero(buffer, 64);
		ft_memcpy(buffer, task->name + copied_l, (length > 64 ? 64 : length));
		copied_l += (length > 64 ? 64 : length);
		state.bit_length = (length >= 64 ? 64 : length) * 8;
		if (state.bit_length < 0)
			state.bit_length = 0;
		state.total_bit_length += state.bit_length;
		md5_round(buffer, &state, length < 56);
		if (state.last_bit_placed && length < 56)
			break ;
		length = length - 64 < 0 ? 0 : length - 64;
	}
	ft_memcpy(hash, state.hash_values, 16);
	return ((void *)1);
}
