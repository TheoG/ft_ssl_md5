/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   md5_stdin.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <tgros@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/10 14:35:59 by tgros             #+#    #+#             */
/*   Updated: 2018/11/15 17:28:13 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"

void	*md5_from_stdin(t_task *task, char *hash)
{
	char		buffer[64];
	char		full_buffer[64];
	uint32_t	remaining_size;
	t_state		state;

	state = *(t_state*)(task->state);
	remaining_size = sizeof(full_buffer);
	ft_bzero(&buffer, 64);
	ft_bzero(&full_buffer, 64);
	md5_stdin_main_loop(task, buffer, full_buffer, &remaining_size);
	state = *(t_state*)(task->state);
	state.bit_length = (64 - remaining_size) * 8;
	state.total_bit_length += state.bit_length;
	md5_stdin_finish(remaining_size, full_buffer, &state, task);
	ft_memcpy(hash, state.hash_values, 16);
	return ((void *)1);
}

void	md5_stdin_finish(uint32_t remaining_size, char *full_buffer,
											t_state *state, t_task *task)
{
	if (remaining_size > 8)
	{
		md5_round(full_buffer, state, true);
		ft_lstadd_end(&task->stdin,
						ft_lstnew(full_buffer, state->bit_length / 8));
	}
	else
	{
		md5_round(full_buffer, state, false);
		ft_lstadd_end(&task->stdin,
						ft_lstnew(full_buffer, state->bit_length / 8));
		ft_bzero(full_buffer, 64);
		state->bit_length = 0;
		md5_round(full_buffer, state, true);
	}
}

void	md5_stdin_main_loop(t_task *task, char *buffer,
												char *fb, uint32_t *size)
{
	while ((((t_state*)task->state)->bit_length = read(0, buffer, 64)))
	{
		while (((t_state*)task->state)->bit_length > 0)
		{
			if ((int)(*size - ((t_state*)task->state)->bit_length) > 0)
			{
				ft_memcpy(fb + 64 - *size, buffer,
						((t_state*)task->state)->bit_length);
				*size -= ((t_state*)task->state)->bit_length;
				((t_state*)task->state)->bit_length = 0;
			}
			else
				md5_stdin_fb_round(task, buffer, fb, size);
		}
	}
}

void	md5_stdin_fb_round(t_task *task, char *buffer,
												char *fb, uint32_t *size)
{
	int			second_copy_part;

	second_copy_part = ((t_state*)task->state)->bit_length - *size;
	ft_memcpy(fb + 64 - *size, buffer, *size);
	((t_state*)task->state)->bit_length = 512;
	((t_state*)task->state)->total_bit_length += 512;
	md5_round(buffer, ((t_state*)task->state), false);
	((t_state*)task->state)->bit_length = second_copy_part;
	ft_lstadd_end(&task->stdin, ft_lstnew(fb, 64));
	ft_memcpy(fb, buffer, second_copy_part);
	*size = 64 - second_copy_part;
}
