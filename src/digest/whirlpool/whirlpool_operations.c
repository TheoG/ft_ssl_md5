/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   whirlpool_operations.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <tgros@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/15 16:31:14 by tgros             #+#    #+#             */
/*   Updated: 2018/11/15 16:52:51 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"

uint64_t	right_or_64(uint64_t a, uint64_t n)
{
	return (((a) >> (n)) | ((a) << (64 - (n))));
}

void		round_function(uint64_t *l, uint64_t *buf, uint64_t i, uint64_t v)
{
	*l = g_whirlpool_t[(buf[i] >> 56) & 0xFF];
	*l ^= right_or_64(g_whirlpool_t[(buf[(i + 7) % 8] >> 48) & 0xFF], 8);
	*l ^= right_or_64(g_whirlpool_t[(buf[(i + 6) % 8] >> 40) & 0xFF], 16);
	*l ^= right_or_64(g_whirlpool_t[(buf[(i + 5) % 8] >> 32) & 0xFF], 24);
	*l ^= right_or_64(g_whirlpool_t[(buf[(i + 4) % 8] >> 24) & 0xFF], 32);
	*l ^= right_or_64(g_whirlpool_t[(buf[(i + 3) % 8] >> 16) & 0xFF], 40);
	*l ^= right_or_64(g_whirlpool_t[(buf[(i + 2) % 8] >> 8) & 0xFF], 48);
	*l ^= right_or_64(g_whirlpool_t[buf[(i + 1) % 8] & 0xFF], 56);
	*l ^= v;
}
