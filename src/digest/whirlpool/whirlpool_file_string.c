/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   whirlpool_file_string.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <tgros@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/15 16:19:35 by tgros             #+#    #+#             */
/*   Updated: 2018/11/16 12:58:11 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"

void	*whirlpool_from_file(t_task *task, char *hash)
{
	int					fd;
	char				buffer[64];
	t_whirlpool_state	state;

	if (file_error_check(&fd, task))
		return (false);
	state = *(t_whirlpool_state*)(task->state);
	ft_bzero(&buffer, 64);
	while ((state.bit_length = read(fd, &buffer, 64)) >= 0)
	{
		buffer[state.bit_length] = 0;
		state.bit_length *= 8;
		state.total_bit_length[1] += state.bit_length;
		whirlpool_round(buffer, &state, state.bit_length == 0
								|| state.bit_length < 256);
		ft_bzero(&buffer, 64);
		if (state.bit_length == 0 || state.bit_length < 256)
		{
			ft_memcpy(hash, state.hash_values, 64);
			break ;
		}
	}
	close(fd);
	return ((void *)1);
}

void	*whirlpool_from_string(t_task *task, char *hash)
{
	t_whirlpool_state	state;
	char				buffer[64];
	int32_t				length;
	int32_t				cpy_len;

	state = *(t_whirlpool_state*)(task->state);
	ft_bzero(&buffer, 64);
	length = ft_strlen(task->name);
	cpy_len = 0;
	while (length >= 0)
	{
		ft_bzero(buffer, 64);
		ft_memcpy(buffer, task->name + cpy_len, (length > 64 ? 64 : length));
		cpy_len += (length > 64 ? 64 : length);
		state.bit_length = (length >= 64 ? 64 : length) * 8;
		if (state.bit_length < 0)
			state.bit_length = 0;
		state.total_bit_length[1] += state.bit_length;
		whirlpool_round(buffer, &state, length < 32);
		if (state.last_bit_placed && length < 32)
			break ;
		length = length - 64 < 0 ? 0 : length - 64;
	}
	ft_memcpy(hash, state.hash_values, 64);
	return ((void *)1);
}
