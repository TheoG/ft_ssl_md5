/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   whirlpool_stdin.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <tgros@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/10 16:00:12 by tgros             #+#    #+#             */
/*   Updated: 2018/11/16 10:52:16 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"

void	*whirlpool_from_stdin(t_task *task, char *hash)
{
	char				buffer[64];
	char				full_buffer[64];
	uint32_t			remaining_size;
	t_whirlpool_state	*state;

	state = (t_whirlpool_state*)(task->state);
	remaining_size = sizeof(full_buffer);
	ft_bzero(&buffer, 64);
	ft_bzero(&full_buffer, 64);
	whirlpool_stdin_main_loop(task, buffer, full_buffer, &remaining_size);
	state->bit_length = (64 - remaining_size) * 8;
	state->total_bit_length[1] += state->bit_length;
	whirlpool_round(full_buffer, state, remaining_size > 32);
	ft_lstadd_end(&task->stdin, ft_lstnew(full_buffer, state->bit_length / 8));
	if (remaining_size <= 32)
	{
		ft_bzero(full_buffer, 64);
		state->bit_length = 0;
		whirlpool_round(full_buffer, state, true);
	}
	ft_memcpy(hash, state->hash_values, 64);
	return ((void *)1);
}

void	whirlpool_stdin_full_buf(t_task *t, uint32_t *size,
								char *buffer, char *fb)
{
	int					second_copy_part;
	t_whirlpool_state	*s;

	s = (t_whirlpool_state*)t->state;
	second_copy_part = s->bit_length - *size;
	ft_memcpy(fb + 64 - *size, buffer, *size);
	s->bit_length = 512;
	s->total_bit_length[1] += 512;
	whirlpool_round(buffer, s, false);
	s->bit_length = second_copy_part;
	ft_lstadd_end(&t->stdin, ft_lstnew(fb, 64));
	ft_memcpy(fb, buffer, second_copy_part);
	*size = 64 - second_copy_part;
}

void	whirlpool_stdin_main_loop(t_task *t, char *buffer,
											char *fb, uint32_t *size)
{
	t_whirlpool_state	*s;

	s = (t_whirlpool_state*)t->state;
	while ((s->bit_length = read(0, buffer, 64)))
	{
		while (s->bit_length > 0)
		{
			if ((int)(*size - s->bit_length) > 0)
			{
				ft_memcpy(fb + 64 - *size, buffer, s->bit_length);
				*size -= s->bit_length;
				s->bit_length = 0;
			}
			else
			{
				whirlpool_stdin_full_buf(t, size, buffer, fb);
			}
		}
	}
}
