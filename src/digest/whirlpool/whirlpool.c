/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   whirlpool.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <tgros@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/02 13:11:49 by tgros             #+#    #+#             */
/*   Updated: 2018/11/16 15:35:41 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"

void	*init_whirlpool_state(t_task *task, char *hash)
{
	t_whirlpool_state	*state;

	(void)task;
	(void)hash;
	if (!(state = (t_whirlpool_state*)malloc(sizeof(t_whirlpool_state))))
	{
		ft_error_exit("Can't allocate the hash state");
	}
	state->hash_values[0] = DEFAULT_H0;
	state->hash_values[1] = DEFAULT_H1;
	state->hash_values[2] = DEFAULT_H2;
	state->hash_values[3] = DEFAULT_H3;
	state->hash_values[4] = DEFAULT_H4;
	state->hash_values[5] = DEFAULT_H5;
	state->hash_values[6] = DEFAULT_H6;
	state->hash_values[7] = DEFAULT_H7;
	ft_bzero(state->h, sizeof(state->h));
	state->bit_length = 0;
	ft_bzero(state->total_bit_length, 32);
	state->last_bit_placed = false;
	state->buffer_size = 16;
	return (state);
}

void	whirlpool_round(char *buffer, t_whirlpool_state *state, bool last)
{
	u_char		hash[64];
	uint32_t	j;
	u_char		*shifted_buffer;

	ft_bzero(hash, 64);
	ft_memcpy(hash, buffer, state->bit_length / 8);
	if (state->bit_length / 8 < 64 && !state->last_bit_placed)
	{
		hash[state->bit_length / 8] = (char)(1 << 7);
		state->last_bit_placed = true;
	}
	shifted_buffer = hash;
	whirlpool_padding(state, last, shifted_buffer);
	j = -1;
	while (++j < 8)
		state->k[j] = state->h[j];
	j = -1;
	while (++j < 8)
		state->state[j] = state->x[j] ^ state->k[j];
	whirlpool_iterations(state);
	j = -1;
	while (++j < 8)
		state->h[j] ^= state->state[j] ^ state->x[j];
	ft_memcpy(state->hash_values, state->h, 64);
}

void	whirlpool_padding(t_whirlpool_state *state, bool last, u_char *buffer)
{
	uint32_t	i;

	i = 0;
	while (i < 8)
	{
		state->x[i] =
			(((uint64_t)buffer[0]) << 56) ^
			(((uint64_t)buffer[1] & 0xffL) << 48) ^
			(((uint64_t)buffer[2] & 0xffL) << 40) ^
			(((uint64_t)buffer[3] & 0xffL) << 32) ^
			(((uint64_t)buffer[4] & 0xffL) << 24) ^
			(((uint64_t)buffer[5] & 0xffL) << 16) ^
			(((uint64_t)buffer[6] & 0xffL) << 8) ^
			(((uint64_t)buffer[7] & 0xffL));
		i++;
		buffer += 8;
	}
	if (last)
		state->x[7] = state->total_bit_length[1];
}

void	whirlpool_iterations(t_whirlpool_state *state)
{
	uint32_t	i;
	uint32_t	j;

	i = 0;
	while (i < 10)
	{
		round_function(&state->l[0], state->k, 0, g_whirlpool_rc[i]);
		j = 0;
		while (++j < 8)
			round_function(&state->l[j], state->k, j, 0);
		j = -1;
		while (++j < 8)
			state->k[j] = state->l[j];
		j = -1;
		while (++j < 8)
			round_function(&state->l[j], state->state, j, state->k[j]);
		j = -1;
		while (++j < 8)
			state->state[j] = state->l[j];
		i++;
	}
}
