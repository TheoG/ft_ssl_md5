/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <tgros@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/13 16:16:38 by tgros             #+#    #+#             */
/*   Updated: 2018/11/16 13:50:11 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"

t_d_parsing	parse(int argc, char *argv[], t_digest digest)
{
	int			i;
	bool		first_file;
	t_d_parsing	parsing;

	i = -1;
	ft_bzero(&parsing, sizeof(t_d_parsing));
	parsing.digest = digest;
	first_file = false;
	while (++i < argc)
	{
		if (!search_options(&parsing, first_file, argv[i]))
		{
			if (argv[i][0] == '-' && argv[i][1] == 's' && !first_file)
			{
				if (create_string_tasks(argv, argc, &parsing, &i))
					break ;
			}
			else if (ft_strcmp(argv[i], g_digest_opts[2]) == 0)
				add_test_suite(&parsing);
			else
				create_stdin_file_tasks(argv[i], &first_file, &parsing);
		}
	}
	create_default_task(&parsing);
	return (parsing);
}

bool		search_options(t_d_parsing *parsing, bool first_file, char *argv)
{
	int		j;

	j = -1;
	while (!first_file && ++j < NUM_DIGEST_OPTS)
	{
		if (ft_strcmp(argv, g_digest_opts[j]) == 0)
		{
			parsing->options |= g_digest_opts_values[j];
			return (true);
		}
	}
	return (false);
}
