/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha384_file_string.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <tgros@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/15 15:30:47 by tgros             #+#    #+#             */
/*   Updated: 2018/11/16 12:57:20 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"

void	*sha384_from_file(t_task *task, char *hash)
{
	int				fd;
	char			buffer[128];
	t_sha384_state	state;

	if (file_error_check(&fd, task))
		return (false);
	state = *(t_sha384_state*)(task->state);
	ft_bzero(&buffer, 128);
	while ((state.bit_length = read(fd, &buffer, 128)) >= 0)
	{
		buffer[state.bit_length] = 0;
		state.bit_length *= 8;
		state.total_bit_length += state.bit_length;
		sha384_round(buffer, &state, state.bit_length == 0
								|| state.bit_length < 896);
		ft_bzero(&buffer, 128);
		if (state.bit_length == 0 || state.bit_length < 896)
		{
			ft_memcpy(hash, state.hash_values, 64);
			break ;
		}
	}
	close(fd);
	return ((void *)1);
}

void	*sha384_from_string(t_task *task, char *hash)
{
	t_sha384_state	state;
	char			buffer[128];
	int32_t			len;
	int32_t			cpy_len;

	state = *(t_sha384_state*)(task->state);
	ft_bzero(&buffer, 128);
	len = ft_strlen(task->name);
	cpy_len = 0;
	while (len >= 0)
	{
		ft_bzero(buffer, 128);
		ft_memcpy(buffer, task->name + cpy_len, (len > 128 ? 128 : len));
		cpy_len += (len > 128 ? 128 : len);
		state.bit_length = (len >= 128 ? 128 : len) * 8;
		if (state.bit_length < 0)
			state.bit_length = 0;
		state.total_bit_length += state.bit_length;
		sha384_round(buffer, &state, len < 112);
		if (state.last_bit_placed && len < 112)
			break ;
		len = len - 128 < 0 ? 0 : len - 128;
	}
	ft_memcpy(hash, state.hash_values, 128);
	return ((void *)1);
}
