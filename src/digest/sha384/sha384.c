/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha384.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <tgros@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/02 13:11:49 by tgros             #+#    #+#             */
/*   Updated: 2018/11/16 15:35:24 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"

void	*init_sha384_state(t_task *task, char *hash)
{
	t_sha384_state	*state;

	(void)task;
	(void)hash;
	if (!(state = (t_sha384_state*)malloc(sizeof(t_sha384_state))))
	{
		ft_error_exit("Can't allocate the hash state");
	}
	state->hash_values[0] = DEFAULT_384_H0;
	state->hash_values[1] = DEFAULT_384_H1;
	state->hash_values[2] = DEFAULT_384_H2;
	state->hash_values[3] = DEFAULT_384_H3;
	state->hash_values[4] = DEFAULT_384_H4;
	state->hash_values[5] = DEFAULT_384_H5;
	state->hash_values[6] = DEFAULT_384_H6;
	state->hash_values[7] = DEFAULT_384_H7;
	state->bit_length = 0;
	state->total_bit_length = 0;
	state->last_bit_placed = false;
	state->buffer_size = SHA384_STATE_SIZE;
	return (state);
}

void	sha384_round(char *buffer, t_sha384_state *state, bool last)
{
	u_char		hash[128];
	uint64_t	tmp_hash[SHA384_STATE_SIZE];
	uint64_t	words[80];
	uint64_t	values[8];

	ft_bzero(hash, 128);
	ft_memcpy(hash, buffer, state->bit_length / 8);
	sha384_end_data(hash, state, last, tmp_hash);
	ft_bzero(values, sizeof(values));
	sha384_setup_word(values, hash, words);
	sha384_transform(values, tmp_hash, words);
	state->hash_values[0] += tmp_hash[0];
	state->hash_values[1] += tmp_hash[1];
	state->hash_values[2] += tmp_hash[2];
	state->hash_values[3] += tmp_hash[3];
	state->hash_values[4] += tmp_hash[4];
	state->hash_values[5] += tmp_hash[5];
	state->hash_values[6] += tmp_hash[6];
	state->hash_values[7] += tmp_hash[7];
}

void	sha384_setup_word(uint64_t *values, u_char *hash, uint64_t *words)
{
	while (values[6] < 16)
	{
		ft_memcpy(&values[0], &hash[0] + values[7], 8);
		swap_bytes(&values[0], sizeof(values[0]));
		words[values[6]] = values[0];
		values[6]++;
		values[7] += 8;
	}
	while (values[6] < 80)
	{
		values[0] = (right_rotate_64(words[values[6] - 15], 1)) ^
					(right_rotate_64(words[values[6] - 15], 8)) ^
					(words[values[6] - 15] >> 7);
		values[1] = (right_rotate_64(words[values[6] - 2], 19)) ^
					(right_rotate_64(words[values[6] - 2], 61)) ^
					(words[values[6] - 2] >> 6);
		words[values[6]] = words[values[6] - 16] + values[0] +
							words[values[6] - 7] + values[1];
		values[6]++;
	}
}

void	sha384_end_data(u_char *hash, t_sha384_state *state, bool last,
													uint64_t *tmp_hash)
{
	__uint128_t swapped;

	if (state->bit_length / 8 < 128 && !state->last_bit_placed)
	{
		hash[state->bit_length / 8] = (char)(1 << 7);
		state->last_bit_placed = true;
	}
	if (last)
	{
		swapped = state->total_bit_length;
		swap_bytes(&swapped, sizeof(swapped));
		ft_memcpy(hash + 112, &swapped, 16);
	}
	ft_memcpy(tmp_hash, state->hash_values,
		sizeof(uint64_t) * SHA384_STATE_SIZE);
}

void	sha384_transform(uint64_t *values, uint64_t *tmp_hash, uint64_t *words)
{
	values[6] = 0;
	while (values[6] < 80)
	{
		values[1] = sigma1_sha384(tmp_hash[4]);
		values[2] = ch_sha384(tmp_hash[4], tmp_hash[5], tmp_hash[6]);
		values[3] = tmp_hash[7] + values[1] + values[2] +
				g_sha384_k[values[6]] + words[values[6]];
		values[0] = sigma0_sha384(tmp_hash[0]);
		values[4] = maj_sha384(tmp_hash[0], tmp_hash[1], tmp_hash[2]);
		values[5] = values[0] + values[4];
		tmp_hash[7] = tmp_hash[6];
		tmp_hash[6] = tmp_hash[5];
		tmp_hash[5] = tmp_hash[4];
		tmp_hash[4] = tmp_hash[3] + values[3];
		tmp_hash[3] = tmp_hash[2];
		tmp_hash[2] = tmp_hash[1];
		tmp_hash[1] = tmp_hash[0];
		tmp_hash[0] = values[3] + values[5];
		values[6]++;
	}
}
