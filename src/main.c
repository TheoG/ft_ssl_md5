/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <tgros@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/02 11:49:06 by tgros             #+#    #+#             */
/*   Updated: 2018/11/16 00:35:19 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"

int		main(int argc, char *argv[])
{
	if (argc < 2)
		not_enough_args();
	else
		check_for_type(argc, argv);
	return (0);
}

void	check_for_type(int argc, char *argv[])
{
	int		j;
	bool	ok;

	j = 0;
	ok = false;
	while (j < DIGEST_E_END)
	{
		if (ft_strcmp(argv[1], g_digests[j]) == 0)
		{
			digest((t_digest)j, argc - 2, argv + 2);
			ok = true;
		}
		j++;
	}
	if (!ok)
		wrong_command(argv[1]);
}

void	not_enough_args(void)
{
	ft_putendl("usage: ft_ssl command [command opts] [command args]");
}

void	wrong_command(char *command)
{
	size_t	i;

	i = -1;
	ft_putstr("ft_ssl: Error: '");
	ft_putstr(command);
	ft_putendl("' is an invalid command\n");
	ft_putendl("Standard commands:\n");
	ft_putendl("Message Digest commands:");
	while (g_digests[++i] != 0)
	{
		ft_putchar('\t');
		ft_putendl(g_digests[i]);
	}
	ft_putendl("Cipher commands:\n");
}
